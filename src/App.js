import "./App.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Layout from "./Components/Layout";
import Notfound from "./Components/404";
import AllTasks from "./Components/AllTasks";
import ActiveTasks from "./Components/ActiveTasks";
import CompletedTasks from "./Components/CompletedTasks";
import { useEffect, useState } from "react";
import TaskListContext from "./context/TaskListContext";
import taskList_DUMMY from "./data/taskList.json";
function App() {
  const taskListLocalStorage = JSON.parse(localStorage.getItem("taskList"));
console.log("taskListLocalStorage:",taskListLocalStorage)
  const [taskList, setTaskList] = useState(
    taskListLocalStorage && taskListLocalStorage.length > 0 ? taskListLocalStorage : taskList_DUMMY
  );
  function updateTaskList(taskL) {
    setTaskList(taskL);
  }

  useEffect(() => {
    console.log("here");
    localStorage.setItem("taskList", JSON.stringify(taskList));
  }, [taskList]);

  /* useEffect(() => {
    const handleTabClosing = (event) => {
    
      localStorage.removeItem("taskList");

    };

    const alertUser = (event) => {
      // localStorage.removeItem("taskList");
      event.preventDefault();
      event.returnValue = "Are you sure you want to exit?";
    };
    window.addEventListener("beforeunload", alertUser);
    window.addEventListener("unload", handleTabClosing);
    return () => {
      window.removeEventListener("beforeunload", alertUser);
      window.removeEventListener("unload", handleTabClosing);
    };
  }); */

  return (
    <div className="App">
      <TaskListContext.Provider value={{ taskList, updateTaskList }}>
        <BrowserRouter>
          <Routes>
            <Route path="*" element={<Notfound />}></Route>
            <Route path="/" element={<Layout />}>
              <Route index element={<Navigate to="allTasks" replace />} />
              <Route path="/allTasks" element={<AllTasks />} />
              <Route path="/activeTasks" element={<ActiveTasks />} />
              <Route path="/completedTasks" element={<CompletedTasks />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </TaskListContext.Provider>
    </div>
  );
}

export default App;
