import { NavLink, Outlet } from "react-router-dom";
import "./css/Layout.css";
import { useState } from "react";
export default function Layout() {
  const [activeLink, setActiveLink] = useState("allTasks");
  const setAllActive = () => {
    setActiveLink("allTasks");
  };
  const setActiveTaskslActive = () => {
    setActiveLink("activeTasks");
  };
  const setCompletedTasksActive = () => {
    setActiveLink("completedTasks");
  };
  return (
    <div className="layout">
      <h1>#todo</h1>
      <div className="filter_title">
        <NavLink
          onClick={setAllActive}
          className={activeLink === "allTasks" && "activeLink"}
          to="/allTasks"
        >
          All
        </NavLink>
        <NavLink
          onClick={setActiveTaskslActive}
          className={activeLink === "activeTasks" && "activeLink"}
          to="/activeTasks"
        >
          Active
        </NavLink>
        <NavLink
          onClick={setCompletedTasksActive}
          className={activeLink === "completedTasks" && "activeLink"}
          to="/completedTasks"
        >
          Completed
        </NavLink>
      </div>
      <Outlet />
    </div>
  );
}
