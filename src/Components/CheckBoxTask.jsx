import { useContext } from "react";
import TaskListContext from "../context/TaskListContext";

export default function CheckBoxTask(props) {
  const { taskList, updateTaskList } = useContext(TaskListContext);
  const changTaskStatus = () => {
    const newTaskList = taskList.map((task) => {
      if (task.id === props.taskID) {
        if (task.status === true) task.status = false;
        else task.status = true;
      }
    });
    updateTaskList(newTaskList)
  };
  return <input type="checkbox" checked onChange={changTaskStatus}></input>;
}
