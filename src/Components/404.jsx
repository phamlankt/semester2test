import { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";

function Notfound(props) {
  const navigate = useNavigate();

  useEffect(() => {
    setTimeout(() => {
      navigate("/");
    }, 3000);
  });
  return (
    <div>
      <h3>NotFound</h3>
      <button
        onClick={() => {
          navigate("/");
        }}
      >
        Home Page
      </button>
    </div>
  );
}
export default Notfound;
