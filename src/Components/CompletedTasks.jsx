import { useContext } from "react";
// import "./css/CompletedTasks.css";
import "./css/AllTasks.css";
import TaskListContext from "../context/TaskListContext";
import CheckBoxTask from "./CheckBoxTask";
export default function CompletedTasks() {
  const { taskList, updateTaskList } = useContext(TaskListContext);
  const deleteAllCompletedTask = (e) => {
    e.preventDefault()
    const newTaskList = taskList.filter((task) => task.status === false);
    console.log("newTaskList:",newTaskList)
    updateTaskList(newTaskList);
  };
  function removeTask(idx) {
    const filterTaskList = taskList.filter((task) => task.id !== idx);
    console.log(filterTaskList);
    updateTaskList(filterTaskList);
    
  }
  return (
    <div className="layout">
      <div className="taskListWrapper">
        {taskList
          .filter((task) => task.status)
          .map((task) => {
            return (
              <div className="taskWrapper" key={task.id}>
                <input type="checkbox" checked readOnly></input>
                {/* <CheckBoxTask/> */}
                <span className={task.status && "crossThrough"}>
                  {task.name}
                </span>
                <button
                  className="taskRemoveBtn"
                  onClick={(e) => {
                    e.preventDefault();
                    removeTask(task.id);
                  }}
                >
                  Remove
                </button>
              </div>
            );
          })}
      </div>
      {taskList.filter((task) => task.status).length > 0 && (
        <div className="deleteAll_btn_wrapper">
          <button className="deleteAll_btn" onClick={deleteAllCompletedTask}>
            Delete all
          </button>
        </div>
      )}
    </div>
  );
}
