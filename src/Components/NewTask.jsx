import { useContext, useState } from "react";
import "./css/AllTasks.css";
import TaskListContext from "../context/TaskListContext";
export default function NewTask() {
  const [newTaskName, setNewTaskName] = useState("");
  const { taskList, updateTaskList } = useContext(TaskListContext);
  const newTaskHandle = (e) => {
    setNewTaskName(e.target.value);
  };
  const addNewTask = () => {
    const newTask = {
      id: Math.floor(Math.random() * 10000),
      name: newTaskName,
      status: false,
    };
    setNewTaskName("")
    const newTaskList = [...taskList,newTask];
    updateTaskList(newTaskList)
    
  };
  return (
    <div className="newTaskSection">
      <input
        className="newTaskInput"
        placeholder="add details"
        value={newTaskName}
        onChange={newTaskHandle}
      ></input>
      <button className="addTaskButton" onClick={addNewTask}>
        Add
      </button>
    </div>
  );
}
