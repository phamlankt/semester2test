import { useContext } from "react";
import TaskListContext from "../context/TaskListContext";
import "./css/AllTasks.css";
import NewTask from "./NewTask";
export default function ActiveTasks() {
  const { taskList, updateTaskList } = useContext(TaskListContext);
  const taskStatusChangeHandle = (tID) => {
    const newTaskList = taskList.map((task) => {
      if (task.id !== tID) return task;
      else {
        console.log("here");
        const newTask = {
          ...task,
          status: task.status ? false : true,
        };
        console.log("newTask:", newTask);
        return newTask;
      }
    });
    console.log("newTaskList:", newTaskList);
    updateTaskList(newTaskList);
  };
  return (
    <div>
      <NewTask />
      <div className="taskListWrapper">
        {taskList
          .filter((task) => !task.status)
          .map((task) => {
            return (
              <div className="taskWrapper" key={task.id}>
                <input
                  type="checkbox"
                  onChange={(e) => {
                    taskStatusChangeHandle(task.id);
                  }}
                ></input>
                <span className={task.status && "crossThrough"}>
                  {task.name}
                </span>
              </div>
            );
          })}
      </div>
    </div>
  );
}
